package org.bitbucket.guikeller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "org.bitbucket.guikeller")
public class App {

    private static final Logger LOGGER = LogManager.getLogger(App.class);

    @Autowired
    private ChessBoard chessBoard;

    protected App(){
        super();
    }

    public static void main( String[] args ) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(App.class);
        App app = applicationContext.getBean(App.class);
        app.run();
    }

    protected void run() {
        chessBoard.startGame();
        LOGGER.info(ChessBoard.getCurrentBoardState());
    }

}
