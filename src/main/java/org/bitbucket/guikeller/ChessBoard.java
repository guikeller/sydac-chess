package org.bitbucket.guikeller;

import com.google.common.collect.Lists;
import org.bitbucket.guikeller.factory.PieceFactory;
import org.bitbucket.guikeller.pieces.base.ChessPiece;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ChessBoard {

    private static final int BOARD_POSITIONS = 8;

    private static final String STARTING_BOARD = "BR,BN,BB,BQ,BK,BB,BN,BR,BP,BP,BP,BP,BP,BP,BP,BP,E,E,E,E,E,E,E,E,E,E,E,E," +
            "E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,WP,WP,WP,WP,WP,WP,WP,WP,WR,WN,WB,WQ,WK,WB,WN,WR";

    private static final List<List<ChessPiece>> BOARD_STATE = new ArrayList<>();


    @Autowired
    private PieceFactory pieceFactory;

    public ChessBoard(){
        super();
    }

    public void startGame(){
        BOARD_STATE.clear();
        BOARD_STATE.addAll(Lists.partition(initialPieces(), BOARD_POSITIONS));
    }

    protected List<ChessPiece> initialPieces(){
        List<ChessPiece> chessPieces = new ArrayList<>();
        String[] startingPieces = STARTING_BOARD.split(",");
        Arrays.stream(startingPieces).forEach((value) -> chessPieces.add(pieceFactory.createPiece(value)));
        return chessPieces;
    }

    public static List<List<ChessPiece>> getCurrentBoardState(){
        return BOARD_STATE;
    }

}
