package org.bitbucket.guikeller.command;

import lombok.Data;

@Data
public class Direction {

    private int x;
    private int y;

    public Direction(int x, int y){
        this.x = x;
        this.y = y;
    }

}
