package org.bitbucket.guikeller.command;

public interface Piece {

    public void move(Direction direction);

}
