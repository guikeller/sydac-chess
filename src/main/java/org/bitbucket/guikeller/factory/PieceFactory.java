package org.bitbucket.guikeller.factory;

import org.bitbucket.guikeller.pieces.*;
import org.bitbucket.guikeller.pieces.base.ChessPiece;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class PieceFactory {

    @Autowired
    private ApplicationContext applicationContext;

    public ChessPiece createPiece(String colourAndPiece){
        if(colourAndPiece.length() == 2){
            String colour = getColour(colourAndPiece);
            String piece = getPiece(colourAndPiece);
            switch (piece){
                case "R":
                    return applicationContext.getBean(Rook.class).withColour(colour);
                case "N":
                    return applicationContext.getBean(Knight.class).withColour(colour);
                case "B":
                    return applicationContext.getBean(Bishop.class).withColour(colour);
                case "Q":
                    return applicationContext.getBean(Queen.class).withColour(colour);
                case "K":
                    return applicationContext.getBean(King.class).withColour(colour);
                case "P":
                    return applicationContext.getBean(Pawn.class).withColour(colour);
                default:
                    throw new IllegalStateException("Unknown chess piece: "+piece);
            }
        } else {
            // Here I could access the board, see the previous colour on list and create an Empty Space with the opposite colour
            // However, as we are keeping things simple all empty spaces are going to be black.
            return new EmptySpace().withColour("B");
        }
    }

    protected String getColour(String colourAndPiece){
        if (colourAndPiece != null && colourAndPiece.length() == 2) {
            return colourAndPiece.substring(0, 1);
        }
        throw new IllegalStateException("This method can only be used from a String with 2 chars! var: "+colourAndPiece);
    }

    protected String getPiece(String colourAndPiece){
        if (colourAndPiece != null && colourAndPiece.length() == 2) {
            return colourAndPiece.substring(1, 2);
        }
        throw new IllegalStateException("This method can only be used from a String with 2 chars! var: "+colourAndPiece);
    }

}
