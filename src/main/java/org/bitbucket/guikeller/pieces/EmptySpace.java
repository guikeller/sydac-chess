package org.bitbucket.guikeller.pieces;

import org.bitbucket.guikeller.command.Direction;
import org.bitbucket.guikeller.pieces.base.ChessPiece;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class EmptySpace extends ChessPiece<EmptySpace> {

    @Override
    public void move(Direction direction) {
        throw new IllegalStateException("Empty Space cannot move! Direction: "+direction);
    }

}
