package org.bitbucket.guikeller.pieces.base;

import lombok.Data;
import org.bitbucket.guikeller.command.Piece;

@Data
public abstract class ChessPiece<T> implements Piece {

    protected ChessPieceColour colour;

    @SuppressWarnings("unchecked")
    public T withColour(String colour){
        this.colour = ChessPieceColour.fromString(colour);
        return (T)this;
    }

}
