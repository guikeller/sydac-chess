package org.bitbucket.guikeller.pieces.base;

import java.util.Arrays;
import java.util.Optional;

public enum ChessPieceColour {

    BLACK("B"),
    WHITE("W");

    private String colour;

    ChessPieceColour(String colour){
        this.colour = colour;
    }

    public static ChessPieceColour fromString(String colour){
        Optional<ChessPieceColour> pieceColour = Arrays.stream(ChessPieceColour.values())
                .filter(v -> v.colour.equalsIgnoreCase(colour))
                .findFirst();
        if(pieceColour.isPresent()){
            return pieceColour.get();
        }else{
            throw new IllegalArgumentException("Unknown colour: "+colour);
        }
    }

}
