package org.bitbucket.guikeller.pieces;

import org.bitbucket.guikeller.command.Direction;
import org.bitbucket.guikeller.pieces.base.ChessPiece;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Knight extends ChessPiece<Knight> {

    @Override
    public void move(Direction direction) {
        // ChessBoard.getCurrentBoardState();
        throw new IllegalStateException("Not implemented");
    }

}
