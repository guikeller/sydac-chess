package org.bitbucket.guikeller.pieces;

import org.bitbucket.guikeller.ChessBoard;
import org.bitbucket.guikeller.command.Direction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ChessBoard.class)
public class KnightTest {

    @Test
    public void testMove(){
        try {
            PowerMockito.mockStatic(ChessBoard.class);
            Knight knight = new Knight().withColour("B");
            knight.move(new Direction(2, 2));
        } catch (IllegalStateException ise) {
            assertNotNull(ise);
            assertTrue("Not implemented".equals(ise.getMessage()));
            PowerMockito.verifyStatic(ChessBoard.class);
            PowerMockito.verifyNoMoreInteractions();
        }
    }

}
