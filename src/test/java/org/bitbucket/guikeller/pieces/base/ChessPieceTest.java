package org.bitbucket.guikeller.pieces.base;

import org.bitbucket.guikeller.pieces.Knight;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ChessPieceColour.class)
public class ChessPieceTest {

    @Test
    public void testConstructorSunnyDay(){
        PowerMockito.mockStatic(ChessPieceColour.class);
        when(ChessPieceColour.fromString("B")).thenReturn(ChessPieceColour.BLACK);

        new Knight().withColour("B");
        PowerMockito.verifyStatic(ChessPieceColour.class);
        PowerMockito.verifyNoMoreInteractions();
    }

}

