package org.bitbucket.guikeller.pieces.base;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ChessPieceColourTest {

    @Test
    public void testChessPieceColourBlack(){
        ChessPieceColour pieceColour = ChessPieceColour.fromString("B");
        assertNotNull(pieceColour);
        assertTrue(ChessPieceColour.BLACK.equals(pieceColour));
    }

    @Test
    public void testChessPieceColourWhite(){
        ChessPieceColour pieceColour = ChessPieceColour.fromString("W");
        assertNotNull(pieceColour);
        assertTrue(ChessPieceColour.WHITE.equals(pieceColour));
    }

    @Test
    public void testChessPieceUnknownColour(){
        try {
            ChessPieceColour.fromString("Z");
        } catch(IllegalArgumentException iae){
            assertNotNull(iae);
            assertTrue("Unknown colour: Z".equals(iae.getMessage()));
        }
    }

}
