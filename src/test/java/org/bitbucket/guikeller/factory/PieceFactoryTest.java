package org.bitbucket.guikeller.factory;

import org.bitbucket.guikeller.pieces.*;
import org.bitbucket.guikeller.pieces.base.ChessPiece;
import org.bitbucket.guikeller.pieces.base.ChessPieceColour;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;
import org.springframework.context.ApplicationContext;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PieceFactoryTest {

    @Test
    public void testGetColourSunnyDay(){
        PieceFactory pieceFactory = new PieceFactory();

        String colour = pieceFactory.getColour("WR");
        assertNotNull(colour);
        assertTrue("W".equals(colour));

        colour = pieceFactory.getColour("BK");
        assertNotNull(colour);
        assertTrue("B".equals(colour));
    }

    @Test
    public void testGetColourRainyDay(){
        PieceFactory pieceFactory = new PieceFactory();

        try {
            pieceFactory.getColour("Y");
        }catch(IllegalStateException ise){
            assertNotNull(ise);
            assertTrue(ise.getMessage().equals("This method can only be used from a String with 2 chars! var: Y"));
        }

        try {
            pieceFactory.getColour("ZZZ");
        }catch(IllegalStateException ise){
            assertNotNull(ise);
            assertTrue(ise.getMessage().equals("This method can only be used from a String with 2 chars! var: ZZZ"));
        }

        try {
            pieceFactory.getColour(null);
        }catch(IllegalStateException ise){
            assertNotNull(ise);
            assertTrue(ise.getMessage().equals("This method can only be used from a String with 2 chars! var: null"));
        }
    }

    @Test
    public void testGetPieceSunnyDay(){
        PieceFactory pieceFactory = new PieceFactory();

        String piece = pieceFactory.getPiece("WR");
        assertNotNull(piece);
        assertTrue("R".equals(piece));

        piece = pieceFactory.getPiece("BK");
        assertNotNull(piece);
        assertTrue("K".equals(piece));
    }

    @Test
    public void testGetPieceRainyDay(){
        PieceFactory pieceFactory = new PieceFactory();
        try {
            pieceFactory.getPiece("Y");
        }catch(IllegalStateException ise){
            assertNotNull(ise);
            assertTrue(ise.getMessage().equals("This method can only be used from a String with 2 chars! var: Y"));
        }

        try {
            pieceFactory.getPiece("ZZZ");
        }catch(IllegalStateException ise){
            assertNotNull(ise);
            assertTrue(ise.getMessage().equals("This method can only be used from a String with 2 chars! var: ZZZ"));
        }

        try {
            pieceFactory.getPiece(null);
        }catch(IllegalStateException ise){
            assertNotNull(ise);
            assertTrue(ise.getMessage().equals("This method can only be used from a String with 2 chars! var: null"));
        }
    }

    @Test
    public void testCreateRookPiece() {
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        PieceFactory pieceFactory = new PieceFactory();
        Whitebox.setInternalState(pieceFactory, "applicationContext" , applicationContext);
        // Rook
        when(applicationContext.getBean(Rook.class)).thenReturn(new Rook().withColour("B"));
        ChessPiece blackRook = pieceFactory.createPiece("BR");
        assertNotNull(blackRook);
        assertTrue(ChessPieceColour.BLACK.equals(blackRook.getColour()));
        assertTrue(blackRook instanceof Rook);
    }

    @Test
    public void testCreateKnightPiece() {
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        PieceFactory pieceFactory = new PieceFactory();
        Whitebox.setInternalState(pieceFactory, "applicationContext" , applicationContext);
        // Knight
        when(applicationContext.getBean(Knight.class)).thenReturn(new Knight().withColour("B"));
        ChessPiece blackKnight = pieceFactory.createPiece("BN");
        assertNotNull(blackKnight);
        assertTrue(ChessPieceColour.BLACK.equals(blackKnight.getColour()));
        assertTrue(blackKnight instanceof Knight);
    }

    @Test
    public void testCreateBishopPiece() {
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        PieceFactory pieceFactory = new PieceFactory();
        Whitebox.setInternalState(pieceFactory, "applicationContext" , applicationContext);
        // Bishop
        when(applicationContext.getBean(Bishop.class)).thenReturn(new Bishop().withColour("B"));
        ChessPiece blackBishop = pieceFactory.createPiece("BB");
        assertNotNull(blackBishop);
        assertTrue(ChessPieceColour.BLACK.equals(blackBishop.getColour()));
        assertTrue(blackBishop instanceof Bishop);
    }

    @Test
    public void testCreateQueenPiece() {
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        PieceFactory pieceFactory = new PieceFactory();
        Whitebox.setInternalState(pieceFactory, "applicationContext" , applicationContext);
        // Queen
        when(applicationContext.getBean(Queen.class)).thenReturn(new Queen().withColour("B"));
        ChessPiece blackQueen = pieceFactory.createPiece("BQ");
        assertNotNull(blackQueen);
        assertTrue(ChessPieceColour.BLACK.equals(blackQueen.getColour()));
        assertTrue(blackQueen instanceof Queen);
    }

    @Test
    public void testCreateKingPiece() {
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        PieceFactory pieceFactory = new PieceFactory();
        Whitebox.setInternalState(pieceFactory, "applicationContext" , applicationContext);
        // King
        when(applicationContext.getBean(King.class)).thenReturn(new King().withColour("B"));
        ChessPiece blackKing = pieceFactory.createPiece("BK");
        assertNotNull(blackKing);
        assertTrue(ChessPieceColour.BLACK.equals(blackKing.getColour()));
        assertTrue(blackKing instanceof King);
    }

    @Test
    public void testCreatePawnPiece() {
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        PieceFactory pieceFactory = new PieceFactory();
        Whitebox.setInternalState(pieceFactory, "applicationContext" , applicationContext);
        // Pawn
        when(applicationContext.getBean(Pawn.class)).thenReturn(new Pawn().withColour("B"));
        ChessPiece blackPawn = pieceFactory.createPiece("BP");
        assertNotNull(blackPawn);
        assertTrue(ChessPieceColour.BLACK.equals(blackPawn.getColour()));
        assertTrue(blackPawn instanceof Pawn);
    }

    @Test
    public void testCreateEmptySpace() {
        PieceFactory pieceFactory = new PieceFactory();
        ChessPiece piece = pieceFactory.createPiece("E");
        assertNotNull(piece);
        assertTrue(ChessPieceColour.BLACK.equals(piece.getColour()));
        assertTrue(piece instanceof EmptySpace);
    }

}
