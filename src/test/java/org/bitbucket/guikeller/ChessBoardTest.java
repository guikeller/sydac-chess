package org.bitbucket.guikeller;

import org.bitbucket.guikeller.factory.PieceFactory;
import org.bitbucket.guikeller.pieces.EmptySpace;
import org.bitbucket.guikeller.pieces.base.ChessPiece;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChessBoardTest {

    @Test
    public void testGetBoardState(){
        List<List<ChessPiece>> currentBoardState = ChessBoard.getCurrentBoardState();
        assertNotNull(currentBoardState);
        assertTrue(currentBoardState.isEmpty());
    }

    @Test
    public void testStartGame(){
        PieceFactory pieceFactory = mock(PieceFactory.class);
        when(pieceFactory.createPiece(anyString())).thenReturn(new EmptySpace().withColour("B"));

        ChessBoard chessBoard = new ChessBoard();
        Whitebox.setInternalState(chessBoard, "pieceFactory", pieceFactory);
        chessBoard.startGame();

        List<List<ChessPiece>> currentBoardState = ChessBoard.getCurrentBoardState();
        assertTrue(!currentBoardState.isEmpty());
        assertEquals(8, currentBoardState.size());
        currentBoardState.stream().forEach(v -> assertEquals(8, v.size()));
    }

    @Test
    public void testInitialPieces(){
        PieceFactory pieceFactory = mock(PieceFactory.class);
        when(pieceFactory.createPiece(anyString())).thenReturn(new EmptySpace().withColour("B"));

        ChessBoard chessBoard = new ChessBoard();
        Whitebox.setInternalState(chessBoard, "pieceFactory", pieceFactory);
        List<ChessPiece> chessPieces = chessBoard.initialPieces();

        assertTrue(!chessPieces.isEmpty());
        assertEquals(64, chessPieces.size());
    }

}
