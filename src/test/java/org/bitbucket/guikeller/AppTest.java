package org.bitbucket.guikeller;

import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;

import static org.mockito.Mockito.*;

public class AppTest {

    @Test
    public void testPlay(){
        ChessBoard chessBoard = mock(ChessBoard.class);

        App app = new App();
        Whitebox.setInternalState(app, "chessBoard", chessBoard);
        app.run();

        verify(chessBoard, times(1)).startGame();
    }

}
